#!/bin/sh

if [ ! -d $HOME/.config ]; then
    mkdir -p $HOME/.config;
fi

if [ ! -d $HOME/.config/wall24 ]; then
    mkdir -p $HOME/.config/wall24;
fi

if [ ! -d $HOME/.config/wall24/$1 ]; then
    mkdir -p $HOME/.config/wall24/$1;
fi

cp -r $1 $HOME/.config/wall24/

index=0
for url in $(cat $HOME/.config/wall24/$1/images.txt); do
    wget $url -O $HOME/.config/wall24/$1/$1_$index.jpeg
    index=$(($index+1))
done
